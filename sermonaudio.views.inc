<?php
function sermonaudio_views_data() {
  $data['sermonaudio']['table']['group'] = t('SermonAudio');
  $data['sermonaudio']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  $data['sermonaudio']['sid'] = array(
    'title' => t('Sermon ID'),
    'help' => t('Sermon ID number from SermonAudio.com'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );
  return $data;
}
?>
